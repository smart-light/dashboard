import Vue from 'vue';
import Vuetify from 'vuetify';
import ru from 'vuetify/src/locale/ru';
import colors from "vuetify/lib/util/colors";

Vue.use(Vuetify);

export default new Vuetify({
    lang: {
      locales: { ru },
      current: 'ru',
    },
    theme: {
        dark: true,
        themes: {
            dark: {
                primary: colors.blue.base,
                secondary: colors.indigo.base,
                accent: colors.deepOrange.base,
                error: colors.red.base,
                warning: colors.orange.base,
                info: colors.cyan.base,
                success: colors.green.base
            },
            light: {
                primary: colors.green.base,
                secondary: colors.indigo.base,
                accent: colors.deepOrange.base,
                error: colors.red.base,
                warning: colors.orange.base,
                info: colors.cyan.base,
                success: colors.green.base
            },
        },
    },
});
