/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Light
 */
export class Light {
    id!: string;
    enable!: boolean;
    serialNumber!: string;
}
