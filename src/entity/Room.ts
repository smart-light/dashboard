/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Room
 */
import { Light } from "@/entity/Light";
import { User } from "@/entity/User";
import { Subscription } from "@/entity/Subscription";

export class Room {
    id!: string;
    name!: string;
    title!: string;
    lights!: Light[];
    users!: User[];
    subscription!: Subscription;
}
