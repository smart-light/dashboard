/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class User
 */
export class User {
    id!: string;
    email!: string;
}
