/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Subscription
 */
export class Subscription {
    id!: string;
    activationDate!: Date;
    active!: boolean;
}
