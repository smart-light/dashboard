import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Sandbox from "@/views/Sandbox.vue";

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    redirect: to => ({name: 'Admin'})
  },
  {
    path: '/dev/sandbox/:component',
    name: "Sandbox",
    component: Sandbox
  },
  {
    path: '/admin',
    name: "Admin",
    component: () => import("@/layout/Application.vue"),
    meta: {
      title: 'Dashboard'
    },
    children: [
      {
        path: 'users',
        name: "Users",
        component: () => import("@/views/UsersView.vue"),
        meta: {
          title: 'Users'
        }
      },
      {
        path: "lights",
        name: "Lights",
        component: () => import("@/views/LightsView.vue"),
        meta: {
          title: "Lights"
        }
      },
      {
        path: "subscriptions",
        name: "Subscriptions",
        component: () => import("@/views/SubscriptionsView.vue"),
        meta: {
          title: "Subscriptions"
        }
      },
      {
        path: "rooms",
        name: "Rooms",
        component: () => import("@/views/RoomsView.vue"),
        meta: {
          title: "Rooms"
        },
      }
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import("@/views/LoginView.vue")
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
